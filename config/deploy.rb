# Change these
server '46.101.18.86', port: 22, roles: [:web, :app], primary: true

set :repo_url,        'git@bitbucket.org:yuritomanek/dnslabs-api.git'
set :application,     'dnslabs-api'
set :user,            'deploy'
set :puma_threads,    [4, 16]
set :puma_workers,    0

set :rbenv_type, :user
set :rbenv_ruby, '2.2.3'

# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/opt/www/#{fetch(:application)}"
set :ssh_options,     { forward_agent: true, user: fetch(:user) }

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml config/local_env.yml}
set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle}

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'passenger:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'passenger:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
