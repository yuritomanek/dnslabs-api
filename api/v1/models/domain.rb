class Domain < ActiveRecord::Base

  self.inheritance_column = nil

  attr_accessor :serial

  validates_presence_of :name
  validates_uniqueness_of :name

  belongs_to :user

  has_many :records, dependent: :destroy
  has_many :forwarders, dependent: :destroy

  has_one  :soa_record, class_name: 'SOA'
  has_many :ns_records, class_name: 'NS'
  has_many :mx_records, class_name: 'MX'
  has_many :a_records, class_name: 'A'
  has_many :txt_records, class_name: 'TXT'
  has_many :cname_records, class_name: 'CNAME'
  has_one  :loc_record, class_name: 'LOC'
  has_many :aaaa_records, class_name: 'AAAA'
  has_many :spf_records, class_name: 'SPF'
  has_many :srv_records, class_name: 'SRV'
  has_many :sshfp_records, class_name: 'SSHFP'
  has_many :ptr_records, class_name: 'PTR'

  has_one :whois_record, dependent: :destroy

  validates_presence_of :name
  validates_uniqueness_of :name

  after_create :create_soa_record
  after_create :setup_ns_records
  after_create :check_whois

  SOA_FIELDS = [ :primary_ns, :contact, :refresh, :retry, :expire, :minimum ]
  SOA_FIELDS.each do |f|
    attr_accessor f
    validates_presence_of f, :on => :create
  end

  def create_soa_record
    soa = SOA.new( :domain => self )
    SOA_FIELDS.each do |f|
      soa.send( "#{f}=", send( f ) )
    end
    soa.serial = serial unless serial.nil?
    if soa.save
    else
      puts "==========\n#{soa.errors.messages if soa.errors}\n==========="
    end
  end

  def setup_ns_records
    domain = self
    domain.records.create(name: domain.name, type: "NS", content: "ns1.dnslabs.co", ttl: 7200)
    domain.records.create(name: domain.name, type: "NS", content: "ns2.dnslabs.co", ttl: 7200)
    domain.records.create(name: domain.name, type: "NS", content: "ns3.dnslabs.co", ttl: 7200)
    domain.records.create(name: domain.name, type: "NS", content: "ns4.dnslabs.co", ttl: 7200)
  end

  def check_whois
    domain = self
    record = Whois.whois(domain.name)
    whois_record = domain.whois_record || domain.build_whois_record
    whois_record.status = record.status.class == Array ? record.status.last.to_s : record.status.to_s if record.property_any_supported?(:status)
    whois_record.available = record.available? if record.property_any_supported?(:available?)
    whois_record.registered = record.registered? if record.property_any_supported?(:registered?)
    whois_record.created_on = record.created_on if record.property_any_supported?(:created_on)
    whois_record.updated_on = record.updated_on if record.property_any_supported?(:updated_on)
    whois_record.expires_on = record.expires_on if record.property_any_supported?(:expires_on)
    whois_record.check_on = Time.now
    whois_record.save

    if record.property_any_supported?(:nameservers)
      whois_record.whois_nameservers.destroy_all
      record.nameservers.each do |nameserver|
        whois_record.whois_nameservers.create(name: nameserver.name)
      end
    end

  end

end
