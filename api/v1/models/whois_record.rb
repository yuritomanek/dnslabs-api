class WhoisRecord < ActiveRecord::Base

  belongs_to :domain
  has_many :whois_nameservers, dependent: :destroy

end
