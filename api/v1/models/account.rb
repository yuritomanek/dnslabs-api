class Account < ActiveRecord::Base

  belongs_to :user

  def basic?
    plan.include?("basic")
  end

  def plus?
    plan.include?("plus")
  end

  def unlimited?
    plan.include?("unlimited")
  end

  def plan_limit
    return BASIC_PLAN if basic?
    return PLUS_PLAN if plus?
    return "Unlimited" if unlimited?
  end

  def domains_left
    if basic?
      BASIC_PLAN - self.user.domains.count
    elsif plus?
      PLUS_PLAN - self.user.domains.count
    else
      "&#8734;".html_safe
    end
  end

  def can_create_domain
    unlimited? || domains_left > 0
  end

  def process_payment(token, coupon=nil)
    begin
      account = self
      current_user = account.user

      Stripe.api_key = SECRET_KEY

      customer = Stripe::Customer.create(email: current_user.email, source: token)

      account.customer_id = customer.id
      account.save

      subscription = customer.subscriptions.create(:plan => "basic2015", :coupon => coupon)

      account.subscription_id = subscription.id
      account.save

      return true
    rescue
      return false
    end
  end

  def process_upgrade
    begin
      account = self
      current_user = account.user

      Stripe.api_key = SECRET_KEY

      customer = Stripe::Customer.retrieve(account.customer_id)

      subscription = customer.subscriptions.retrieve(account.subscription_id)
      subscription.plan = account.plan
      subscription.save

      return true
    rescue
      return false
    end
  end

end
