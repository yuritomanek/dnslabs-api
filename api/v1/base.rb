require './api/v1/models/record.rb'
Dir["#{File.dirname(__FILE__)}/models/**/*.rb"].each {|f| require f}

module API
  module V1
    class Base < Grape::API

      PRIMARY_NS = "ns1.dnslabs.co"
      CONTACT = "hostmaster@dnslabs.co"
      REFRESH = 10800
      RETRY = 3600
      EXPIRE = 604800
      MINIMUM  = 3600

      version 'v1'
      format :json

      rescue_from ActiveRecord::RecordInvalid do |error|
        error!("No good", 404)
      end

      rescue_from ActiveRecord::RecordInvalid do |error|
        message = error.record.errors.messages.map { |attr, msg| msg.first }
        error!(message.join(", "), 400)
      end

      before do
        error!("401 Unauthorized", 401) unless authenticated
      end

      helpers do

        def authenticated
          user_email = headers['X-Dnslabs-Email'].presence
          api_token = headers["X-Dnslabs-Token"].presence
          @user = user_email && User.find_by_email(user_email)
          @user && Devise.secure_compare(@user.api_token, api_token)
        end

      end

      resource :domains do

        desc "All Domains"
        get do
          cache_control :private, max_age: 60
          domains = @user.domains.select("id,name,records_count,created_at,updated_at").all
          present :domains, domains
        end

        desc "Get a domain"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
        end
        route_param :domain_id do
          get do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            present :domain, domain
          end
        end

        desc "Create domain"
        params do
          requires :name, type: String, desc: 'domain name.'
        end
        post do
          domain = @user.domains.new(name: params[:name])
          domain.type = "NATIVE"
          domain.primary_ns = PRIMARY_NS
          domain.contact = CONTACT
          domain.refresh = REFRESH
          domain.retry = RETRY
          domain.expire = EXPIRE
          domain.minimum = MINIMUM
          if domain.save
            domain = @user.domains.where(id: domain.id).select("id,name,records_count,created_at,updated_at").first
            present :domain, domain
          else
            status 4009
            present :error, "Domain #{domain.errors.messages[:name][0]}"
          end
        end

        desc "Delete a domain"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
        end
        route_param :domain_id do
          delete do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            if domain && domain.destroy
              status 204
            else
              status 400
              present :error
            end
          end
        end

        desc "Get all records"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
        end
        route_param :domain_id do
          get "records" do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            records = domain.records.select("id,name,content,ttl,type as record_type,prio,created_at,updated_at") if domain
            present :domain, domain
            present :records, records
          end
        end

        desc "Get record"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
        end
        route_param :domain_id do
          get "records/:record_id" do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            record = domain.records.where(id: params[:record_id]).select("id,name,content,ttl,type as record_type,prio,created_at,updated_at").first if domain
            present :domain, domain
            present :record, record
          end
        end

        desc "Create record"
        params do
          requires :id, type: Integer, desc: 'domain id.'
          requires :name, type: String, desc: 'record name.'
          requires :content, type: String, desc: 'record content.'
          requires :ttl, type: Integer, desc: 'record ttl.'
          requires :record_type, type: String, desc: 'record type.'
        end
        route_param :id do
          post :record do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:id]).first
            record = domain.records.create(name: params[:name], content: params[:content], ttl: params[:ttl], type: params[:record_type].upcase) if domain
            record = domain.records.where(id: record.id).select("id,name,content,ttl,type as record_type,prio,created_at,updated_at").first
            present :domain, domain
            present :record, record
          end
        end

        desc "Update record"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
          requires :name, type: String, desc: 'record name.'
          requires :content, type: String, desc: 'record content.'
        end
        route_param :domain_id do
          put "records/:record_id" do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            record = domain.records.where(id: params[:record_id]).first if domain
            record.name = params[:name]
            record.content = params[:content]
            if record.save
              record = domain.records.where(id: record.id).select("id,name,content,ttl,type as record_type,prio,created_at,updated_at").first
              present :domain, domain
              present :record, record
            else
              status 400
              present :error
            end
          end
        end

        desc "Delete record"
        params do
          requires :domain_id, type: Integer, desc: 'domain id.'
        end
        route_param :domain_id do
          delete "records/:record_id" do
            domain = @user.domains.select("id,name,records_count,created_at,updated_at").where(id: params[:domain_id]).first
            record = domain.records.where(id: params[:record_id]).first if domain
            if record && record.destroy
              status 204
            else
              status 400
              present :error
            end
          end
        end

      end

    end
  end
end
