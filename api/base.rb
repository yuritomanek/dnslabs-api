require './api/validators/record_patterns.rb'
Dir["#{File.dirname(__FILE__)}/validators/**/*.rb"].each {|f| require f}
require './api/v1/base'

module API
  class Base < Grape::API

    use Rack::SslEnforcer

    format :json

    desc "root"
    get "/" do
      cache_control :public, max_age: 900
      present :site, "DNS Labs :: API - http://dnslabs.net"
    end

    desc "ssl"
    get "/ssl" do
      cache_control :public, max_age: 900
      present :error, "SSL required"
    end

    desc "ping"
    get "/ping" do
      present "pong"
    end

    mount API::V1::Base

  end
end
