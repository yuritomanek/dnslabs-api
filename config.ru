require 'rubygems'
require 'bundler'
require 'devise'
require 'grape'
require 'newrelic_rpm'
require 'active_record'
require 'grape/cache_control'
require 'rack/ssl-enforcer'
require './api/base'

env_file = File.join('./config/local_env.yml')
YAML.load(File.open(env_file)).each do |key, value|
  ENV[key.to_s] = value
end if File.exists?(env_file)

Bundler.require

use ActiveRecord::ConnectionAdapters::ConnectionManagement

run API::Base

PhusionPassenger.install_framework_extensions! if defined?(PhusionPassenger)
